﻿
namespace Taller1_GM181937
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnResolverEcuacion = new System.Windows.Forms.Button();
            this.btnEncontrarVotos = new System.Windows.Forms.Button();
            this.btnCalcularMayoMenor = new System.Windows.Forms.Button();
            this.btnCalcularPago = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Escoja la opción de lo que desea realizar:";
            // 
            // btnResolverEcuacion
            // 
            this.btnResolverEcuacion.Location = new System.Drawing.Point(12, 41);
            this.btnResolverEcuacion.Name = "btnResolverEcuacion";
            this.btnResolverEcuacion.Size = new System.Drawing.Size(126, 54);
            this.btnResolverEcuacion.TabIndex = 1;
            this.btnResolverEcuacion.Text = "Resolver ecuación cuadrática";
            this.btnResolverEcuacion.UseVisualStyleBackColor = true;
            this.btnResolverEcuacion.Click += new System.EventHandler(this.btnResolverEcuacion_Click);
            // 
            // btnEncontrarVotos
            // 
            this.btnEncontrarVotos.Location = new System.Drawing.Point(194, 41);
            this.btnEncontrarVotos.Name = "btnEncontrarVotos";
            this.btnEncontrarVotos.Size = new System.Drawing.Size(126, 54);
            this.btnEncontrarVotos.TabIndex = 2;
            this.btnEncontrarVotos.Text = "Encotrar número de botos";
            this.btnEncontrarVotos.UseVisualStyleBackColor = true;
            this.btnEncontrarVotos.Click += new System.EventHandler(this.btnEncontrarVotos_Click);
            // 
            // btnCalcularMayoMenor
            // 
            this.btnCalcularMayoMenor.Location = new System.Drawing.Point(12, 136);
            this.btnCalcularMayoMenor.Name = "btnCalcularMayoMenor";
            this.btnCalcularMayoMenor.Size = new System.Drawing.Size(126, 54);
            this.btnCalcularMayoMenor.TabIndex = 3;
            this.btnCalcularMayoMenor.Text = "calcular número mayor o menor";
            this.btnCalcularMayoMenor.UseVisualStyleBackColor = true;
            this.btnCalcularMayoMenor.Click += new System.EventHandler(this.btnCalcularMayoMenor_Click);
            // 
            // btnCalcularPago
            // 
            this.btnCalcularPago.Location = new System.Drawing.Point(194, 136);
            this.btnCalcularPago.Name = "btnCalcularPago";
            this.btnCalcularPago.Size = new System.Drawing.Size(126, 54);
            this.btnCalcularPago.TabIndex = 4;
            this.btnCalcularPago.Text = "Calcular pago líquido de empleado";
            this.btnCalcularPago.UseVisualStyleBackColor = true;
            this.btnCalcularPago.Click += new System.EventHandler(this.btnCalcularPago_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(128, 211);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 5;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 246);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnCalcularPago);
            this.Controls.Add(this.btnCalcularMayoMenor);
            this.Controls.Add(this.btnEncontrarVotos);
            this.Controls.Add(this.btnResolverEcuacion);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Menú";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnResolverEcuacion;
        private System.Windows.Forms.Button btnEncontrarVotos;
        private System.Windows.Forms.Button btnCalcularMayoMenor;
        private System.Windows.Forms.Button btnCalcularPago;
        private System.Windows.Forms.Button btnSalir;
    }
}

