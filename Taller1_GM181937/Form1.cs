﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Taller1_GM181937
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnResolverEcuacion_Click(object sender, EventArgs e)
        {
            //Abrimos el formulario que se encargará de realizar el cálculo de la ecuación cuadrática
            EcuacionCuadratica frmEcuacionCuadratica = new EcuacionCuadratica();
            frmEcuacionCuadratica.Visible = true;
        }

        private void btnEncontrarVotos_Click(object sender, EventArgs e)
        {
            //Abrimos el formulario de encontrar votos
            EncontrarVotos frmEncontrarVotos = new EncontrarVotos();
            frmEncontrarVotos.Visible = true;
        }

        private void btnCalcularPago_Click(object sender, EventArgs e)
        {
            //Abrimos el formulario de pago empleados
            PagoEmpleado frmPagoEmpleado = new PagoEmpleado();
            frmPagoEmpleado.Visible = true;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCalcularMayoMenor_Click(object sender, EventArgs e)
        {
            CalcularNumero frmCalcularNumero = new CalcularNumero();
            frmCalcularNumero.Visible = true;
        }
    }
}
