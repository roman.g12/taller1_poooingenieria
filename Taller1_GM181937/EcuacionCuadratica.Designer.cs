﻿
namespace Taller1_GM181937
{
    partial class EcuacionCuadratica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFormula = new System.Windows.Forms.Label();
            this.lblIngresarDatos = new System.Windows.Forms.Label();
            this.txtValorA = new System.Windows.Forms.TextBox();
            this.txtValorB = new System.Windows.Forms.TextBox();
            this.txtValorC = new System.Windows.Forms.TextBox();
            this.lblA = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.lblC = new System.Windows.Forms.Label();
            this.lblX1 = new System.Windows.Forms.Label();
            this.lblX2 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFormula
            // 
            this.lblFormula.AutoSize = true;
            this.lblFormula.Location = new System.Drawing.Point(119, 18);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(113, 14);
            this.lblFormula.TabIndex = 0;
            this.lblFormula.Text = "Fórmula cuadrática:";
            // 
            // lblIngresarDatos
            // 
            this.lblIngresarDatos.AutoSize = true;
            this.lblIngresarDatos.Location = new System.Drawing.Point(12, 44);
            this.lblIngresarDatos.Name = "lblIngresarDatos";
            this.lblIngresarDatos.Size = new System.Drawing.Size(161, 14);
            this.lblIngresarDatos.TabIndex = 1;
            this.lblIngresarDatos.Text = "Ingrese los datos necesarios:";
            // 
            // txtValorA
            // 
            this.txtValorA.Location = new System.Drawing.Point(43, 74);
            this.txtValorA.Name = "txtValorA";
            this.txtValorA.Size = new System.Drawing.Size(45, 23);
            this.txtValorA.TabIndex = 2;
            // 
            // txtValorB
            // 
            this.txtValorB.Location = new System.Drawing.Point(153, 74);
            this.txtValorB.Name = "txtValorB";
            this.txtValorB.Size = new System.Drawing.Size(45, 23);
            this.txtValorB.TabIndex = 3;
            // 
            // txtValorC
            // 
            this.txtValorC.Location = new System.Drawing.Point(261, 74);
            this.txtValorC.Name = "txtValorC";
            this.txtValorC.Size = new System.Drawing.Size(45, 23);
            this.txtValorC.TabIndex = 4;
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(21, 77);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(16, 14);
            this.lblA.TabIndex = 5;
            this.lblA.Text = "a:";
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Location = new System.Drawing.Point(131, 77);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(17, 14);
            this.lblB.TabIndex = 6;
            this.lblB.Text = "b:";
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.Location = new System.Drawing.Point(238, 77);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(16, 14);
            this.lblC.TabIndex = 7;
            this.lblC.Text = "c:";
            // 
            // lblX1
            // 
            this.lblX1.AutoSize = true;
            this.lblX1.Location = new System.Drawing.Point(21, 132);
            this.lblX1.Name = "lblX1";
            this.lblX1.Size = new System.Drawing.Size(21, 14);
            this.lblX1.TabIndex = 8;
            this.lblX1.Text = "X1:";
            // 
            // lblX2
            // 
            this.lblX2.AutoSize = true;
            this.lblX2.Location = new System.Drawing.Point(21, 170);
            this.lblX2.Name = "lblX2";
            this.lblX2.Size = new System.Drawing.Size(23, 14);
            this.lblX2.TabIndex = 9;
            this.lblX2.Text = "X2:";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(231, 161);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 10;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // EcuacionCuadratica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 205);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblX2);
            this.Controls.Add(this.lblX1);
            this.Controls.Add(this.lblC);
            this.Controls.Add(this.lblB);
            this.Controls.Add(this.lblA);
            this.Controls.Add(this.txtValorC);
            this.Controls.Add(this.txtValorB);
            this.Controls.Add(this.txtValorA);
            this.Controls.Add(this.lblIngresarDatos);
            this.Controls.Add(this.lblFormula);
            this.Name = "EcuacionCuadratica";
            this.Text = "EcuacionCuadratica";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.Label lblIngresarDatos;
        private System.Windows.Forms.TextBox txtValorA;
        private System.Windows.Forms.TextBox txtValorB;
        private System.Windows.Forms.TextBox txtValorC;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lblX1;
        private System.Windows.Forms.Label lblX2;
        private System.Windows.Forms.Button btnCalcular;
    }
}