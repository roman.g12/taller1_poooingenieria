﻿
namespace Taller1_GM181937
{
    partial class PagoEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblITituloIngresarDatos = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.lblCargoEmpleado = new System.Windows.Forms.Label();
            this.lblHoras = new System.Windows.Forms.Label();
            this.txtCargoEmpleados = new System.Windows.Forms.TextBox();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.txtHoras = new System.Windows.Forms.TextBox();
            this.btnIngresarEmpleado = new System.Windows.Forms.Button();
            this.btnCalcularpagos = new System.Windows.Forms.Button();
            this.lblEmpleado1 = new System.Windows.Forms.Label();
            this.lblEmpleado2 = new System.Windows.Forms.Label();
            this.lblEmpleado3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblISSS1 = new System.Windows.Forms.Label();
            this.lblAFP1 = new System.Windows.Forms.Label();
            this.lblRENTA1 = new System.Windows.Forms.Label();
            this.lblISSS2 = new System.Windows.Forms.Label();
            this.lblAFP2 = new System.Windows.Forms.Label();
            this.lblRENTA2 = new System.Windows.Forms.Label();
            this.lblISSS3 = new System.Windows.Forms.Label();
            this.lblAFP3 = new System.Windows.Forms.Label();
            this.lblRENTA3 = new System.Windows.Forms.Label();
            this.lblSueldo1 = new System.Windows.Forms.Label();
            this.lblSueldo2 = new System.Windows.Forms.Label();
            this.lblSueldo3 = new System.Windows.Forms.Label();
            this.lblTituloMayorSalario = new System.Windows.Forms.Label();
            this.lblTituloMenorSalario = new System.Windows.Forms.Label();
            this.lblMayorSalario = new System.Windows.Forms.Label();
            this.lblMenorSalario = new System.Windows.Forms.Label();
            this.lblMayorTrecientos = new System.Windows.Forms.Label();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblITituloIngresarDatos
            // 
            this.lblITituloIngresarDatos.AutoSize = true;
            this.lblITituloIngresarDatos.Location = new System.Drawing.Point(12, 9);
            this.lblITituloIngresarDatos.Name = "lblITituloIngresarDatos";
            this.lblITituloIngresarDatos.Size = new System.Drawing.Size(218, 14);
            this.lblITituloIngresarDatos.TabIndex = 0;
            this.lblITituloIngresarDatos.Text = "Ingrese los datos de los tres empleados:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(12, 40);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(53, 14);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Location = new System.Drawing.Point(12, 78);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(59, 14);
            this.lblApellidos.TabIndex = 2;
            this.lblApellidos.Text = "Apellidos:";
            // 
            // lblCargoEmpleado
            // 
            this.lblCargoEmpleado.AutoSize = true;
            this.lblCargoEmpleado.Location = new System.Drawing.Point(12, 117);
            this.lblCargoEmpleado.Name = "lblCargoEmpleado";
            this.lblCargoEmpleado.Size = new System.Drawing.Size(113, 14);
            this.lblCargoEmpleado.TabIndex = 3;
            this.lblCargoEmpleado.Text = "Cargo del empleado:";
            // 
            // lblHoras
            // 
            this.lblHoras.AutoSize = true;
            this.lblHoras.Location = new System.Drawing.Point(12, 157);
            this.lblHoras.Name = "lblHoras";
            this.lblHoras.Size = new System.Drawing.Size(97, 14);
            this.lblHoras.TabIndex = 4;
            this.lblHoras.Text = "Horas laboradas:";
            // 
            // txtCargoEmpleados
            // 
            this.txtCargoEmpleados.Location = new System.Drawing.Point(131, 114);
            this.txtCargoEmpleados.Name = "txtCargoEmpleados";
            this.txtCargoEmpleados.Size = new System.Drawing.Size(153, 23);
            this.txtCargoEmpleados.TabIndex = 5;
            // 
            // txtNombres
            // 
            this.txtNombres.Location = new System.Drawing.Point(131, 40);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(153, 23);
            this.txtNombres.TabIndex = 6;
            // 
            // txtApellidos
            // 
            this.txtApellidos.Location = new System.Drawing.Point(131, 75);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(153, 23);
            this.txtApellidos.TabIndex = 7;
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(131, 154);
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(66, 23);
            this.txtHoras.TabIndex = 8;
            // 
            // btnIngresarEmpleado
            // 
            this.btnIngresarEmpleado.Location = new System.Drawing.Point(12, 210);
            this.btnIngresarEmpleado.Name = "btnIngresarEmpleado";
            this.btnIngresarEmpleado.Size = new System.Drawing.Size(113, 23);
            this.btnIngresarEmpleado.TabIndex = 9;
            this.btnIngresarEmpleado.Text = "Ingresar empleado";
            this.btnIngresarEmpleado.UseVisualStyleBackColor = true;
            this.btnIngresarEmpleado.Click += new System.EventHandler(this.btnIngresarEmpleado_Click);
            // 
            // btnCalcularpagos
            // 
            this.btnCalcularpagos.Enabled = false;
            this.btnCalcularpagos.Location = new System.Drawing.Point(188, 210);
            this.btnCalcularpagos.Name = "btnCalcularpagos";
            this.btnCalcularpagos.Size = new System.Drawing.Size(96, 23);
            this.btnCalcularpagos.TabIndex = 10;
            this.btnCalcularpagos.Text = "Calcular pagos";
            this.btnCalcularpagos.UseVisualStyleBackColor = true;
            this.btnCalcularpagos.Click += new System.EventHandler(this.btnCalcularpagos_Click);
            // 
            // lblEmpleado1
            // 
            this.lblEmpleado1.AutoSize = true;
            this.lblEmpleado1.Location = new System.Drawing.Point(317, 43);
            this.lblEmpleado1.Name = "lblEmpleado1";
            this.lblEmpleado1.Size = new System.Drawing.Size(66, 14);
            this.lblEmpleado1.TabIndex = 11;
            this.lblEmpleado1.Text = "Empleado 1";
            // 
            // lblEmpleado2
            // 
            this.lblEmpleado2.AutoSize = true;
            this.lblEmpleado2.Location = new System.Drawing.Point(317, 83);
            this.lblEmpleado2.Name = "lblEmpleado2";
            this.lblEmpleado2.Size = new System.Drawing.Size(68, 14);
            this.lblEmpleado2.TabIndex = 12;
            this.lblEmpleado2.Text = "Empleado 2";
            // 
            // lblEmpleado3
            // 
            this.lblEmpleado3.AutoSize = true;
            this.lblEmpleado3.Location = new System.Drawing.Point(317, 122);
            this.lblEmpleado3.Name = "lblEmpleado3";
            this.lblEmpleado3.Size = new System.Drawing.Size(68, 14);
            this.lblEmpleado3.TabIndex = 13;
            this.lblEmpleado3.Text = "Empleado 3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(507, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "ISSS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(578, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 14);
            this.label2.TabIndex = 15;
            this.label2.Text = "AFP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(652, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "RENTA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(763, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Sueldo";
            // 
            // lblISSS1
            // 
            this.lblISSS1.AutoSize = true;
            this.lblISSS1.Location = new System.Drawing.Point(507, 42);
            this.lblISSS1.Name = "lblISSS1";
            this.lblISSS1.Size = new System.Drawing.Size(39, 14);
            this.lblISSS1.TabIndex = 18;
            this.lblISSS1.Text = "label5";
            this.lblISSS1.Visible = false;
            // 
            // lblAFP1
            // 
            this.lblAFP1.AutoSize = true;
            this.lblAFP1.Location = new System.Drawing.Point(578, 42);
            this.lblAFP1.Name = "lblAFP1";
            this.lblAFP1.Size = new System.Drawing.Size(38, 14);
            this.lblAFP1.TabIndex = 19;
            this.lblAFP1.Text = "label6";
            this.lblAFP1.Visible = false;
            // 
            // lblRENTA1
            // 
            this.lblRENTA1.AutoSize = true;
            this.lblRENTA1.Location = new System.Drawing.Point(652, 42);
            this.lblRENTA1.Name = "lblRENTA1";
            this.lblRENTA1.Size = new System.Drawing.Size(38, 14);
            this.lblRENTA1.TabIndex = 20;
            this.lblRENTA1.Text = "label7";
            this.lblRENTA1.Visible = false;
            // 
            // lblISSS2
            // 
            this.lblISSS2.AutoSize = true;
            this.lblISSS2.Location = new System.Drawing.Point(507, 82);
            this.lblISSS2.Name = "lblISSS2";
            this.lblISSS2.Size = new System.Drawing.Size(39, 14);
            this.lblISSS2.TabIndex = 21;
            this.lblISSS2.Text = "label8";
            this.lblISSS2.Visible = false;
            // 
            // lblAFP2
            // 
            this.lblAFP2.AutoSize = true;
            this.lblAFP2.Location = new System.Drawing.Point(578, 82);
            this.lblAFP2.Name = "lblAFP2";
            this.lblAFP2.Size = new System.Drawing.Size(38, 14);
            this.lblAFP2.TabIndex = 22;
            this.lblAFP2.Text = "label9";
            this.lblAFP2.Visible = false;
            // 
            // lblRENTA2
            // 
            this.lblRENTA2.AutoSize = true;
            this.lblRENTA2.Location = new System.Drawing.Point(652, 82);
            this.lblRENTA2.Name = "lblRENTA2";
            this.lblRENTA2.Size = new System.Drawing.Size(43, 14);
            this.lblRENTA2.TabIndex = 23;
            this.lblRENTA2.Text = "label10";
            this.lblRENTA2.Visible = false;
            // 
            // lblISSS3
            // 
            this.lblISSS3.AutoSize = true;
            this.lblISSS3.Location = new System.Drawing.Point(507, 121);
            this.lblISSS3.Name = "lblISSS3";
            this.lblISSS3.Size = new System.Drawing.Size(40, 14);
            this.lblISSS3.TabIndex = 24;
            this.lblISSS3.Text = "label11";
            this.lblISSS3.Visible = false;
            // 
            // lblAFP3
            // 
            this.lblAFP3.AutoSize = true;
            this.lblAFP3.Location = new System.Drawing.Point(578, 121);
            this.lblAFP3.Name = "lblAFP3";
            this.lblAFP3.Size = new System.Drawing.Size(42, 14);
            this.lblAFP3.TabIndex = 25;
            this.lblAFP3.Text = "label12";
            this.lblAFP3.Visible = false;
            // 
            // lblRENTA3
            // 
            this.lblRENTA3.AutoSize = true;
            this.lblRENTA3.Location = new System.Drawing.Point(652, 121);
            this.lblRENTA3.Name = "lblRENTA3";
            this.lblRENTA3.Size = new System.Drawing.Size(42, 14);
            this.lblRENTA3.TabIndex = 26;
            this.lblRENTA3.Text = "label13";
            this.lblRENTA3.Visible = false;
            // 
            // lblSueldo1
            // 
            this.lblSueldo1.AutoSize = true;
            this.lblSueldo1.Location = new System.Drawing.Point(764, 39);
            this.lblSueldo1.Name = "lblSueldo1";
            this.lblSueldo1.Size = new System.Drawing.Size(43, 14);
            this.lblSueldo1.TabIndex = 27;
            this.lblSueldo1.Text = "label14";
            this.lblSueldo1.Visible = false;
            // 
            // lblSueldo2
            // 
            this.lblSueldo2.AutoSize = true;
            this.lblSueldo2.Location = new System.Drawing.Point(764, 82);
            this.lblSueldo2.Name = "lblSueldo2";
            this.lblSueldo2.Size = new System.Drawing.Size(43, 14);
            this.lblSueldo2.TabIndex = 28;
            this.lblSueldo2.Text = "label15";
            this.lblSueldo2.Visible = false;
            // 
            // lblSueldo3
            // 
            this.lblSueldo3.AutoSize = true;
            this.lblSueldo3.Location = new System.Drawing.Point(764, 121);
            this.lblSueldo3.Name = "lblSueldo3";
            this.lblSueldo3.Size = new System.Drawing.Size(42, 14);
            this.lblSueldo3.TabIndex = 29;
            this.lblSueldo3.Text = "label16";
            this.lblSueldo3.Visible = false;
            // 
            // lblTituloMayorSalario
            // 
            this.lblTituloMayorSalario.AutoSize = true;
            this.lblTituloMayorSalario.Location = new System.Drawing.Point(317, 175);
            this.lblTituloMayorSalario.Name = "lblTituloMayorSalario";
            this.lblTituloMayorSalario.Size = new System.Drawing.Size(82, 14);
            this.lblTituloMayorSalario.TabIndex = 30;
            this.lblTituloMayorSalario.Text = "Mayor salario:";
            this.lblTituloMayorSalario.Visible = false;
            // 
            // lblTituloMenorSalario
            // 
            this.lblTituloMenorSalario.AutoSize = true;
            this.lblTituloMenorSalario.Location = new System.Drawing.Point(317, 210);
            this.lblTituloMenorSalario.Name = "lblTituloMenorSalario";
            this.lblTituloMenorSalario.Size = new System.Drawing.Size(83, 14);
            this.lblTituloMenorSalario.TabIndex = 31;
            this.lblTituloMenorSalario.Text = "Menor salario:";
            this.lblTituloMenorSalario.Visible = false;
            // 
            // lblMayorSalario
            // 
            this.lblMayorSalario.AutoSize = true;
            this.lblMayorSalario.Location = new System.Drawing.Point(406, 175);
            this.lblMayorSalario.Name = "lblMayorSalario";
            this.lblMayorSalario.Size = new System.Drawing.Size(38, 14);
            this.lblMayorSalario.TabIndex = 32;
            this.lblMayorSalario.Text = "label6";
            this.lblMayorSalario.Visible = false;
            // 
            // lblMenorSalario
            // 
            this.lblMenorSalario.AutoSize = true;
            this.lblMenorSalario.Location = new System.Drawing.Point(407, 209);
            this.lblMenorSalario.Name = "lblMenorSalario";
            this.lblMenorSalario.Size = new System.Drawing.Size(38, 14);
            this.lblMenorSalario.TabIndex = 33;
            this.lblMenorSalario.Text = "label7";
            this.lblMenorSalario.Visible = false;
            // 
            // lblMayorTrecientos
            // 
            this.lblMayorTrecientos.AutoSize = true;
            this.lblMayorTrecientos.Location = new System.Drawing.Point(564, 188);
            this.lblMayorTrecientos.Name = "lblMayorTrecientos";
            this.lblMayorTrecientos.Size = new System.Drawing.Size(38, 14);
            this.lblMayorTrecientos.TabIndex = 34;
            this.lblMayorTrecientos.Text = "label6";
            this.lblMayorTrecientos.Visible = false;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Enabled = false;
            this.btnLimpiar.Location = new System.Drawing.Point(786, 226);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 35;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // PagoEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 261);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.lblMayorTrecientos);
            this.Controls.Add(this.lblMenorSalario);
            this.Controls.Add(this.lblMayorSalario);
            this.Controls.Add(this.lblTituloMenorSalario);
            this.Controls.Add(this.lblTituloMayorSalario);
            this.Controls.Add(this.lblSueldo3);
            this.Controls.Add(this.lblSueldo2);
            this.Controls.Add(this.lblSueldo1);
            this.Controls.Add(this.lblRENTA3);
            this.Controls.Add(this.lblAFP3);
            this.Controls.Add(this.lblISSS3);
            this.Controls.Add(this.lblRENTA2);
            this.Controls.Add(this.lblAFP2);
            this.Controls.Add(this.lblISSS2);
            this.Controls.Add(this.lblRENTA1);
            this.Controls.Add(this.lblAFP1);
            this.Controls.Add(this.lblISSS1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblEmpleado3);
            this.Controls.Add(this.lblEmpleado2);
            this.Controls.Add(this.lblEmpleado1);
            this.Controls.Add(this.btnCalcularpagos);
            this.Controls.Add(this.btnIngresarEmpleado);
            this.Controls.Add(this.txtHoras);
            this.Controls.Add(this.txtApellidos);
            this.Controls.Add(this.txtNombres);
            this.Controls.Add(this.txtCargoEmpleados);
            this.Controls.Add(this.lblHoras);
            this.Controls.Add(this.lblCargoEmpleado);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblITituloIngresarDatos);
            this.Name = "PagoEmpleado";
            this.Text = "PagoEmpleado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblITituloIngresarDatos;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label lblCargoEmpleado;
        private System.Windows.Forms.Label lblHoras;
        private System.Windows.Forms.TextBox txtCargoEmpleados;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.TextBox txtHoras;
        private System.Windows.Forms.Button btnIngresarEmpleado;
        private System.Windows.Forms.Button btnCalcularpagos;
        private System.Windows.Forms.Label lblEmpleado1;
        private System.Windows.Forms.Label lblEmpleado2;
        private System.Windows.Forms.Label lblEmpleado3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblISSS1;
        private System.Windows.Forms.Label lblAFP1;
        private System.Windows.Forms.Label lblRENTA1;
        private System.Windows.Forms.Label lblISSS2;
        private System.Windows.Forms.Label lblAFP2;
        private System.Windows.Forms.Label lblRENTA2;
        private System.Windows.Forms.Label lblISSS3;
        private System.Windows.Forms.Label lblAFP3;
        private System.Windows.Forms.Label lblRENTA3;
        private System.Windows.Forms.Label lblSueldo1;
        private System.Windows.Forms.Label lblSueldo2;
        private System.Windows.Forms.Label lblSueldo3;
        private System.Windows.Forms.Label lblTituloMayorSalario;
        private System.Windows.Forms.Label lblTituloMenorSalario;
        private System.Windows.Forms.Label lblMayorSalario;
        private System.Windows.Forms.Label lblMenorSalario;
        private System.Windows.Forms.Label lblMayorTrecientos;
        private System.Windows.Forms.Button btnLimpiar;
    }
}