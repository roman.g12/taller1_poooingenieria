﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Taller1_GM181937
{
    public partial class PagoEmpleado : Form
    {
        private int index = 0;
        string[] nombres = new string[3];
        string[] apellidos = new string[3];
        string[] cargo = new string[3];
        int[] horas = new int[3];
        public PagoEmpleado()
        {
            InitializeComponent();
        }

        private void btnIngresarEmpleado_Click(object sender, EventArgs e)
        {
            //Validamos todos los campos
            if (txtNombres.Text != "" && txtApellidos.Text != "" && txtCargoEmpleados.Text != "")
            {
                if (txtHoras.Text != "")
                {
                    if (int.Parse(txtHoras.Text) != 0 && int.Parse(txtHoras.Text) > 0)
                    {
                        //Obtenemos el valor de las txt
                        nombres[index] = txtNombres.Text;
                        apellidos[index] = txtApellidos.Text;
                        cargo[index] = txtCargoEmpleados.Text;
                        horas[index] = int.Parse(txtHoras.Text);
                        //Vamos mostrando los usuarios en el sistema
                        if (index == 0) { lblEmpleado1.Text = nombres[index] + " " + apellidos[index]; }
                        else if (index == 1) { lblEmpleado2.Text = nombres[index] + " " + apellidos[index]; }
                        else if (index == 2)
                        {
                            lblEmpleado3.Text = nombres[index] + apellidos[index];

                            btnIngresarEmpleado.Enabled = false;
                            btnCalcularpagos.Enabled = true;
                            index = 0;
                        }

                        index++;
                        //Limpiamos las txt
                        txtNombres.Text = "";
                        txtApellidos.Text = "";
                        txtHoras.Text = "";
                        txtCargoEmpleados.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("¡ERROR! las horas deben ser mayor a 0");
                    }
                }
                else
                {
                    MessageBox.Show("¡ERROR! No debe dejar campos vacíos");
                }
            }
            else
            {
                MessageBox.Show("¡ERROR! No puede dejar campos vacíos");
            }
        }

        private void btnCalcularpagos_Click(object sender, EventArgs e)
        {
            double[] bono = new double[3];
            double[] salarioNeto = new double[3];
            double[] salarioBase = new double[3];
            double[] descuentoISS = new double[3];
            double[] descuentoAFP = new double[3];
            double[] descuentoRENTA = new double[3];
            bool bandera; //bandera que nos servira para saber si hay bonos o no

            //Calculando el sueldo base según horas trabajadas
            for (int i = 0; i < horas.Length; i++)
            {
                if (horas[i] <= 160)
                {
                    salarioBase[i] = 9.75 * horas[i];
                }
                else if (horas[i] > 160)
                {
                    salarioBase[i] = (9.75 * 160) + ((horas[i] - 160) * 11.50);
                }
            }

            //Descuentos
            for (int i = 0; i < salarioBase.Length; i++)
            {
                descuentoISS[i] = salarioBase[i] * 0.0525;
                descuentoAFP[i] = salarioBase[i] * 0.0688;
                descuentoRENTA[i] = salarioBase[i] * 0.1;
            }

            //Validamos los cargos para calcular los bonos
            if (cargo[0].ToUpper() == "GERENTE" && cargo[1].ToUpper() == "ASISTENTE" && cargo[2].ToUpper() == "SECRETARIA")
            {
                MessageBox.Show("NO HAY BONO");
                bandera = false;
            }
            else
            {
                bandera = true;
                for (int i = 0; i < cargo.Length; i++)
                {
                    if (cargo[i].ToUpper() == "GERENTE")
                    {
                        bono[i] = 0.1;
                    }
                    else if (cargo[i].ToUpper() == "ASISTENTE")
                    {
                        bono[i] = 0.05;
                    }
                    else if (cargo[i].ToUpper() == "SECRETARIA")
                    {
                        bono[i] = 0.03;
                    }
                    else
                    {
                        bono[i] = 0.02;
                    }
                }
            }

            //Calculamos los salarios netos de cada empleado
            for (int i = 0; i < salarioBase.Length; i++)
            {
                if (bandera == true)
                {
                    salarioNeto[i] = salarioBase[i] - descuentoISS[i] - descuentoAFP[i] - descuentoRENTA[i];
                    salarioNeto[i] += salarioNeto[i] * bono[i];
                }
                else
                {
                    salarioNeto[i] = salarioBase[i] - descuentoISS[i] - descuentoAFP[i] - descuentoRENTA[i];
                }
            }

            //Mostramos datos al usuario

            //Descuentos ISSS
            lblISSS1.Text = Math.Round(descuentoISS[0], 2).ToString();
            lblISSS2.Text = Math.Round(descuentoISS[1], 2).ToString();
            lblISSS3.Text = Math.Round(descuentoISS[2], 2).ToString();
            lblISSS1.Visible = true;
            lblISSS2.Visible = true;
            lblISSS3.Visible = true;

            //Descuentos AFP
            lblAFP1.Text = Math.Round(descuentoAFP[0], 2).ToString();
            lblAFP2.Text = Math.Round(descuentoAFP[1], 2).ToString();
            lblAFP3.Text = Math.Round(descuentoAFP[2], 2).ToString();
            lblAFP1.Visible = true;
            lblAFP2.Visible = true;
            lblAFP3.Visible = true;

            //Descuentos RENTA
            lblRENTA1.Text = Math.Round(descuentoRENTA[0], 2).ToString();
            lblRENTA2.Text = Math.Round(descuentoRENTA[1], 2).ToString();
            lblRENTA3.Text = Math.Round(descuentoRENTA[2], 2).ToString();
            lblRENTA1.Visible = true;
            lblRENTA2.Visible = true;
            lblRENTA3.Visible = true;

            //Salarios
            lblSueldo1.Text = Math.Round(salarioNeto[0], 2).ToString();
            lblSueldo2.Text = Math.Round(salarioNeto[1], 2).ToString();
            lblSueldo3.Text = Math.Round(salarioNeto[2], 2).ToString();
            lblSueldo1.Visible = true;
            lblSueldo2.Visible = true;
            lblSueldo3.Visible = true;

            //Mostrando quien tiene mayor y menor salario
            if (salarioNeto[0] > salarioNeto[1] && salarioNeto[0] > salarioNeto[2])
            {
                lblTituloMayorSalario.Visible = true;
                lblMayorSalario.Text = nombres[0] + " " + apellidos[0];
                lblMayorSalario.Visible = true;
            }
            else if (salarioNeto[1] > salarioNeto[0] && salarioNeto[1] > salarioNeto[2])
            {
                lblTituloMayorSalario.Visible = true;
                lblMayorSalario.Text = nombres[1] + " " + apellidos[1];
                lblMayorSalario.Visible = true;
            }
            else if (salarioNeto[2] > salarioNeto[0] && salarioNeto[2] > salarioNeto[1])
            {
                lblTituloMayorSalario.Visible = true;
                lblMayorSalario.Text = nombres[2] + " " + apellidos[2];
                lblMayorSalario.Visible = true;
            }

            //Mostrando quien tiene menor salario
            if (salarioNeto[0] < salarioNeto[1] && salarioNeto[0] < salarioNeto[2])
            {
                lblTituloMenorSalario.Visible = true;
                lblMenorSalario.Text = nombres[0] + " " + apellidos[0];
                lblMenorSalario.Visible = true;
            }
            else if (salarioNeto[1] < salarioNeto[0] && salarioNeto[1] < salarioNeto[2])
            {
                lblTituloMenorSalario.Visible = true;
                lblMenorSalario.Text = nombres[1] + " " + apellidos[1];
                lblMenorSalario.Visible = true;
            }
            else if (salarioNeto[2] < salarioNeto[0] && salarioNeto[2] < salarioNeto[1])
            {
                lblTituloMenorSalario.Visible = true;
                lblMenorSalario.Text = nombres[2] + " " + apellidos[2];
                lblMenorSalario.Visible = true;
            }

            //Mostrando cuántos ganan más de 300
            int contadorSalarios = 0;
            if (salarioNeto[0] > 300) { contadorSalarios++; }
            if (salarioNeto[0] > 300) { contadorSalarios++; }
            if (salarioNeto[0] > 300) { contadorSalarios++; }

            lblMayorTrecientos.Text = contadorSalarios + " de los empleados ingresados ganan más de 300";
            lblMayorTrecientos.Visible = true;

            contadorSalarios = 0;
            btnCalcularpagos.Enabled = false;
            btnLimpiar.Enabled = true;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //Limpiamos todos los labels, reseteamos los button y los txt
            lblEmpleado1.Text = "Empleado 1";
            lblEmpleado2.Text = "Empleado 2";
            lblEmpleado3.Text = "Empleado 3";

            lblISSS1.Visible = false;
            lblISSS2.Visible = false;
            lblISSS3.Visible = false;

            lblAFP1.Visible = false;
            lblAFP2.Visible = false;
            lblAFP3.Visible = false;

            lblRENTA1.Visible = false;
            lblRENTA2.Visible = false;
            lblRENTA3.Visible = false;

            lblSueldo1.Visible = false;
            lblSueldo2.Visible = false;
            lblSueldo3.Visible = false;

            lblMayorSalario.Visible = false;
            lblMenorSalario.Visible = false;
            lblTituloMayorSalario.Visible = false;
            lblTituloMenorSalario.Visible = false;
            lblMayorTrecientos.Visible = false;

            btnIngresarEmpleado.Enabled = true;
            btnLimpiar.Enabled = false;

            index = 0;
        }
    }
}
