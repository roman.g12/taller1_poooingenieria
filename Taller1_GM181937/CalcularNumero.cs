﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Taller1_GM181937
{
    public partial class CalcularNumero : Form
    {
        public CalcularNumero()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (
                String.IsNullOrEmpty(textBox1.Text.Trim()) ||
                String.IsNullOrEmpty(textBox2.Text.Trim()) ||
                String.IsNullOrEmpty(textBox3.Text.Trim()) ||
                String.IsNullOrEmpty(textBox4.Text.Trim())
                )
            { MessageBox.Show("Llene todos los campos."); return; }

            int validateInt;
            if (
                !int.TryParse(textBox1.Text.Trim(), out validateInt) ||
                !int.TryParse(textBox2.Text.Trim(), out validateInt) ||
                !int.TryParse(textBox3.Text.Trim(), out validateInt) ||
                !int.TryParse(textBox4.Text.Trim(), out validateInt)
                )
            { MessageBox.Show("Los datos que debe ingresar deben ser números enteros, no se permiten decimales ni letras."); return; }

            int[] nums = new int[4];
            nums[0] = int.Parse(textBox1.Text.Trim());
            nums[1] = int.Parse(textBox2.Text.Trim());
            nums[2] = int.Parse(textBox3.Text.Trim());
            nums[3] = int.Parse(textBox4.Text.Trim());
            int suma = 0, mayor = nums[0], menor = nums[0];
            String rmayor, rmenor;

            int validateNums = 0;
            for (int i = 0; i < nums.Length; i++) { if (nums[i] <= 0) { ++validateNums; } }
            if (validateNums > 0) { MessageBox.Show("No se aceptan números negativos o iguales a cero"); return; }

            for (int i = 0; i < nums.Length; i++)
            {
                if (mayor <= nums[i]) { mayor = nums[i]; }
                if (menor >= nums[i]) { menor = nums[i]; }
                suma += nums[i];
            }

            if (suma >= 200)
            {
                MessageBox.Show("La suma de los números es mayor o igual a 200, Debe ingresar nuevamente los datos");
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
            }
            else
            {
                if (menor > 10) { rmayor = (mayor + 10).ToString(); } else { rmayor = "No se cumple"; };
                if (mayor < 50) { rmenor = (menor - 5).ToString(); } else { rmenor = "No se cumple"; };

                // MOSTRAR RESULTADOS

                labelMayor.Text = mayor.ToString();
                labelMenor.Text = menor.ToString();
                labelSuma.Text = suma.ToString();
                labelRMenor.Text = rmenor;
                labelRMayor.Text = rmayor;
            }
        }
    }
}
