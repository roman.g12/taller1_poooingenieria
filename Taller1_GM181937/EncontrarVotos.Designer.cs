﻿
namespace Taller1_GM181937
{
    partial class EncontrarVotos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblContadorVotos = new System.Windows.Forms.Label();
            this.lblCandidato1 = new System.Windows.Forms.Label();
            this.lblCandidato2 = new System.Windows.Forms.Label();
            this.lblCandidato3 = new System.Windows.Forms.Label();
            this.lblTituloVoto = new System.Windows.Forms.Label();
            this.lblTituloPorcentaje = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lblVoto1 = new System.Windows.Forms.Label();
            this.lblVoto2 = new System.Windows.Forms.Label();
            this.lblVoto3 = new System.Windows.Forms.Label();
            this.lblPorcentaje1 = new System.Windows.Forms.Label();
            this.lblPorcentaje2 = new System.Windows.Forms.Label();
            this.lblPorcentaje3 = new System.Windows.Forms.Label();
            this.lblCandidato4 = new System.Windows.Forms.Label();
            this.lblVoto4 = new System.Windows.Forms.Label();
            this.lblPorcentaje4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblContadorVotos
            // 
            this.lblContadorVotos.AutoSize = true;
            this.lblContadorVotos.Location = new System.Drawing.Point(12, 9);
            this.lblContadorVotos.Name = "lblContadorVotos";
            this.lblContadorVotos.Size = new System.Drawing.Size(104, 14);
            this.lblContadorVotos.TabIndex = 0;
            this.lblContadorVotos.Text = "Contador de votos";
            // 
            // lblCandidato1
            // 
            this.lblCandidato1.AutoSize = true;
            this.lblCandidato1.Location = new System.Drawing.Point(125, 49);
            this.lblCandidato1.Name = "lblCandidato1";
            this.lblCandidato1.Size = new System.Drawing.Size(67, 14);
            this.lblCandidato1.TabIndex = 1;
            this.lblCandidato1.Text = "Canditato 1:";
            // 
            // lblCandidato2
            // 
            this.lblCandidato2.AutoSize = true;
            this.lblCandidato2.Location = new System.Drawing.Point(242, 49);
            this.lblCandidato2.Name = "lblCandidato2";
            this.lblCandidato2.Size = new System.Drawing.Size(69, 14);
            this.lblCandidato2.TabIndex = 2;
            this.lblCandidato2.Text = "Canditato 2:";
            // 
            // lblCandidato3
            // 
            this.lblCandidato3.AutoSize = true;
            this.lblCandidato3.Location = new System.Drawing.Point(353, 49);
            this.lblCandidato3.Name = "lblCandidato3";
            this.lblCandidato3.Size = new System.Drawing.Size(69, 14);
            this.lblCandidato3.TabIndex = 3;
            this.lblCandidato3.Text = "Canditato 3:";
            // 
            // lblTituloVoto
            // 
            this.lblTituloVoto.AutoSize = true;
            this.lblTituloVoto.Location = new System.Drawing.Point(30, 91);
            this.lblTituloVoto.Name = "lblTituloVoto";
            this.lblTituloVoto.Size = new System.Drawing.Size(40, 14);
            this.lblTituloVoto.TabIndex = 4;
            this.lblTituloVoto.Text = "Votos:";
            // 
            // lblTituloPorcentaje
            // 
            this.lblTituloPorcentaje.AutoSize = true;
            this.lblTituloPorcentaje.Location = new System.Drawing.Point(30, 158);
            this.lblTituloPorcentaje.Name = "lblTituloPorcentaje";
            this.lblTituloPorcentaje.Size = new System.Drawing.Size(66, 14);
            this.lblTituloPorcentaje.TabIndex = 5;
            this.lblTituloPorcentaje.Text = "Porcentaje:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(509, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Encontrar votos";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblVoto1
            // 
            this.lblVoto1.AutoSize = true;
            this.lblVoto1.Location = new System.Drawing.Point(138, 91);
            this.lblVoto1.Name = "lblVoto1";
            this.lblVoto1.Size = new System.Drawing.Size(38, 14);
            this.lblVoto1.TabIndex = 7;
            this.lblVoto1.Text = "label7";
            this.lblVoto1.Visible = false;
            // 
            // lblVoto2
            // 
            this.lblVoto2.AutoSize = true;
            this.lblVoto2.Location = new System.Drawing.Point(254, 91);
            this.lblVoto2.Name = "lblVoto2";
            this.lblVoto2.Size = new System.Drawing.Size(39, 14);
            this.lblVoto2.TabIndex = 8;
            this.lblVoto2.Text = "label8";
            this.lblVoto2.Visible = false;
            // 
            // lblVoto3
            // 
            this.lblVoto3.AutoSize = true;
            this.lblVoto3.Location = new System.Drawing.Point(368, 91);
            this.lblVoto3.Name = "lblVoto3";
            this.lblVoto3.Size = new System.Drawing.Size(38, 14);
            this.lblVoto3.TabIndex = 9;
            this.lblVoto3.Text = "label9";
            this.lblVoto3.Visible = false;
            // 
            // lblPorcentaje1
            // 
            this.lblPorcentaje1.AutoSize = true;
            this.lblPorcentaje1.Location = new System.Drawing.Point(138, 157);
            this.lblPorcentaje1.Name = "lblPorcentaje1";
            this.lblPorcentaje1.Size = new System.Drawing.Size(43, 14);
            this.lblPorcentaje1.TabIndex = 10;
            this.lblPorcentaje1.Text = "label10";
            this.lblPorcentaje1.Visible = false;
            // 
            // lblPorcentaje2
            // 
            this.lblPorcentaje2.AutoSize = true;
            this.lblPorcentaje2.Location = new System.Drawing.Point(254, 158);
            this.lblPorcentaje2.Name = "lblPorcentaje2";
            this.lblPorcentaje2.Size = new System.Drawing.Size(40, 14);
            this.lblPorcentaje2.TabIndex = 11;
            this.lblPorcentaje2.Text = "label11";
            this.lblPorcentaje2.Visible = false;
            // 
            // lblPorcentaje3
            // 
            this.lblPorcentaje3.AutoSize = true;
            this.lblPorcentaje3.Location = new System.Drawing.Point(368, 156);
            this.lblPorcentaje3.Name = "lblPorcentaje3";
            this.lblPorcentaje3.Size = new System.Drawing.Size(42, 14);
            this.lblPorcentaje3.TabIndex = 12;
            this.lblPorcentaje3.Text = "label12";
            this.lblPorcentaje3.Visible = false;
            // 
            // lblCandidato4
            // 
            this.lblCandidato4.AutoSize = true;
            this.lblCandidato4.Location = new System.Drawing.Point(460, 49);
            this.lblCandidato4.Name = "lblCandidato4";
            this.lblCandidato4.Size = new System.Drawing.Size(70, 14);
            this.lblCandidato4.TabIndex = 13;
            this.lblCandidato4.Text = "Canditato 4:";
            // 
            // lblVoto4
            // 
            this.lblVoto4.AutoSize = true;
            this.lblVoto4.Location = new System.Drawing.Point(478, 91);
            this.lblVoto4.Name = "lblVoto4";
            this.lblVoto4.Size = new System.Drawing.Size(36, 14);
            this.lblVoto4.TabIndex = 14;
            this.lblVoto4.Text = "label1";
            this.lblVoto4.Visible = false;
            // 
            // lblPorcentaje4
            // 
            this.lblPorcentaje4.AutoSize = true;
            this.lblPorcentaje4.Location = new System.Drawing.Point(478, 156);
            this.lblPorcentaje4.Name = "lblPorcentaje4";
            this.lblPorcentaje4.Size = new System.Drawing.Size(38, 14);
            this.lblPorcentaje4.TabIndex = 15;
            this.lblPorcentaje4.Text = "label2";
            this.lblPorcentaje4.Visible = false;
            // 
            // EncontrarVotos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 239);
            this.Controls.Add(this.lblPorcentaje4);
            this.Controls.Add(this.lblVoto4);
            this.Controls.Add(this.lblCandidato4);
            this.Controls.Add(this.lblPorcentaje3);
            this.Controls.Add(this.lblPorcentaje2);
            this.Controls.Add(this.lblPorcentaje1);
            this.Controls.Add(this.lblVoto3);
            this.Controls.Add(this.lblVoto2);
            this.Controls.Add(this.lblVoto1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblTituloPorcentaje);
            this.Controls.Add(this.lblTituloVoto);
            this.Controls.Add(this.lblCandidato3);
            this.Controls.Add(this.lblCandidato2);
            this.Controls.Add(this.lblCandidato1);
            this.Controls.Add(this.lblContadorVotos);
            this.Name = "EncontrarVotos";
            this.Text = "EncontrarVotos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContadorVotos;
        private System.Windows.Forms.Label lblCandidato1;
        private System.Windows.Forms.Label lblCandidato2;
        private System.Windows.Forms.Label lblCandidato3;
        private System.Windows.Forms.Label lblTituloVoto;
        private System.Windows.Forms.Label lblTituloPorcentaje;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblVoto1;
        private System.Windows.Forms.Label lblVoto2;
        private System.Windows.Forms.Label lblVoto3;
        private System.Windows.Forms.Label lblPorcentaje1;
        private System.Windows.Forms.Label lblPorcentaje2;
        private System.Windows.Forms.Label lblPorcentaje3;
        private System.Windows.Forms.Label lblCandidato4;
        private System.Windows.Forms.Label lblVoto4;
        private System.Windows.Forms.Label lblPorcentaje4;
    }
}