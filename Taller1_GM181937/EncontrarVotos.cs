﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Taller1_GM181937
{
    public partial class EncontrarVotos : Form
    {
        public EncontrarVotos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Generamos los números a guardar en el txt
            Random aleatorios = new Random();
            int numero, i=1;
            string datosTxt = "", datosRecuperados;
            string[] datosTXTRecuperados;
            float contadorVotos1 = 0, contadorVotos2 = 0, contadorVotos3 = 0, contadorVotos4 = 0;
            float porcentaje1, porcentaje2, porcentaje3, porcentaje4;

            //Generaremos el número 20 veces.
            while (i != 21)
            {
                numero = aleatorios.Next(1, 5); //Generamos el número aleatorio

                if (i == 20)
                {
                    datosTxt += numero.ToString(); //Una variable de tipo string tendrá nuestros datos a guardar en el txt
                }
                else
                {
                    datosTxt += numero.ToString() + ","; //Una variable de tipo string tendrá nuestros datos a guardar en el txt
                }

                i++;
            }

            File.WriteAllText("votos.txt", datosTxt); // Guardamos los datos en el txt

            //Leemos los datos
            datosRecuperados = File.ReadAllText("votos.txt");
            datosTXTRecuperados = datosRecuperados.Split(",");

            for (int j = 0; j < datosTXTRecuperados.Length; j++)
            {
                //Obtenemos la cantidad de votos por candidatos
                if (datosTXTRecuperados[j].Equals("1"))
                {
                    contadorVotos1++;
                } else if (datosTXTRecuperados[j].Equals("2"))
                {
                    contadorVotos2++;
                } else if (datosTXTRecuperados[j].Equals("3"))
                {
                    contadorVotos3++;
                } else if (datosTXTRecuperados[j].Equals("4"))
                {
                    contadorVotos4++;
                }
                //MessageBox.Show(contadorVotos1.ToString());
            }
            //Obtenemos el porcentaje por candidato
            porcentaje1 = contadorVotos1 / 20 * 100;
            porcentaje2 = contadorVotos2 / 20 * 100;
            porcentaje3 = contadorVotos3 / 20 * 100;
            porcentaje4 = contadorVotos4 / 20 * 100;

            porcentaje1 = (int)Math.Round(porcentaje1);
            porcentaje2 = (int)Math.Round(porcentaje2);
            porcentaje3 = (int)Math.Round(porcentaje3);
            porcentaje4 = (int)Math.Round(porcentaje4);

            //Mostramos los resultados en las labels
            //VOTOS
            lblVoto1.Text = contadorVotos1.ToString();
            lblVoto2.Text = contadorVotos2.ToString();
            lblVoto3.Text = contadorVotos3.ToString();
            lblVoto4.Text = contadorVotos4.ToString();
            lblVoto1.Visible = true;
            lblVoto2.Visible = true;
            lblVoto3.Visible = true;
            lblVoto4.Visible = true;
            //PORCENTAJE
            lblPorcentaje1.Text = porcentaje1.ToString() + "%";
            lblPorcentaje2.Text = porcentaje2.ToString() + "%";
            lblPorcentaje3.Text = porcentaje3.ToString() + "%";
            lblPorcentaje4.Text = porcentaje4.ToString() + "%";
            lblPorcentaje1.Visible = true;
            lblPorcentaje2.Visible = true;
            lblPorcentaje3.Visible = true;
            lblPorcentaje4.Visible = true;
        }
    }
}
