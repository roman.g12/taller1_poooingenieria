﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Taller1_GM181937
{
    public partial class EcuacionCuadratica : Form
    {
        public EcuacionCuadratica()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //Guardamos en las variables el valor de las txt
            double x1, x2;
            float valorA = float.Parse(txtValorA.Text);
            float valorB = float.Parse(txtValorB.Text);
            float valorC = float.Parse(txtValorC.Text);

            //utilizamos formula para X1 y X2
            x1 = ((-1) * valorB + Math.Sqrt(((valorB * valorB) - (4 * valorA * valorC)))) / (2 * valorA);
            x2 = ((-1) * valorB - Math.Sqrt(((valorB * valorB) - (4 * valorA * valorC)))) / (2 * valorA);

            lblX1.Text = "X1: " + x1;
            lblX2.Text = "X2: " + x2;
        }
    }
}
